
//Definição dos tipos da mensagem
//---------------------------------

#define CONFIG        (uint8_t) 0x12
#define ACTION		    (uint8_t) 0x30
#define ERRO          (uint8_t) 0x24
#define GET_STATUS    (uint8_t) 0xE5
#define GET_NIVEL     (uint8_t) 0xE6

//Definição do STATE da bomba
//---------------------------------

#define ON_STATE      (uint8_t) 0x79
#define OFF_STATE     (uint8_t) 0x01

//Definição do tamanho da message
//---------------------------------

#define SIZE_HEAD				(uint8_t) 0x03
#define SIZE_CONFIG			(uint8_t) 0x03
#define SIZE_ACTION			(uint8_t) 0x01
#define SIZE_ERRO				(uint8_t) 0x01
#define SIZE_GET_STATUS	(uint8_t) 0x01
#define SIZE_GET_NIVEL	(uint8_t) 0x03

//Definição de posição
//---------------------------------

#define POS_ID					(uint8_t) 0x00
#define POS_SIZE				(uint8_t) 0x01
#define POS_TYPE				(uint8_t) 0x02
#define POS_DATA				(uint8_t) 0x03

//Definição do id dos dispositivos
//---------------------------------

#define uFOG						(uint8_t) 0x01
#define iForninho_ID 		(uint8_t) 0x05
#define iNemo_ID				(uint8_t) 0x10
#define iBombinha_ID		(uint8_t) 0x15

//Definição dos erros
//---------------------------------

#define ERRO_OFFLINE    (uint8_t)	0x42
#define ERRO_CONFIG			(uint8_t) 0x3C
#define ERRO_CRC				(uint8_t) 0x2D

//Lista de pinos
//---------------------------------

#define PIN_RS         16
#define PIN_E          5
#define PIN_D4         4
#define PIN_D5         0
#define PIN_D6         12
#define PIN_D7         14
#define PIN_iBOMBA		 2
#define PIN_SWITCH		15


//Definição do CRC polinomial
//---------------------------------

#define CRC_POLYNOMIAL (uint8_t) 305

//Lista de servers para sntp
//---------------------------------

#define SNTP_SERVERS 	"0.pool.ntp.org", "1.pool.ntp.org", \
						"2.pool.ntp.org", "3.pool.ntp.org"
