#include "Bomba.h"

void init_iBomba(iBomba * iBomba_obj)
{
    gpio_enable(PIN_iBOMBA, GPIO_OUTPUT);
    iBomba_obj->nivel_min = 10;
    iBomba_obj->nivel_max = 100;
    iBomba_obj->nivel_current = 100;
    iBomba_obj->state = OFF_STATE;
}

void setStatus(iBomba *iBomba_obj, uint8_t action)
{
    if(action == ON_STATE)
    {
        gpio_write(PIN_iBOMBA, 1);
        iBomba_obj->state = ON_STATE;
    }
    if(action == OFF_STATE)
    {
        gpio_write(PIN_iBOMBA, 0);
        iBomba_obj->state = OFF_STATE;
    }
}

int isConfigValid(int min, int max)
{
    return max > min;
}

void qtest_bomba(void *pvParameters)
{
    int i=0;
    int p;
    iBomba iBombinha_obj;
    init_iBomba(&iBombinha_obj);
    printf("Iniciando a bomba:");
    while(1)
    {
        p = rand()%11;
        i = 1 - i;
        if(i)
        {
            setStatus(&iBombinha_obj, ON_STATE);
            printf("NIVEL = %d\n", iBombinha_obj.nivel_current);
            printf_nivel_agua(p);
        }
        else
        {
            setStatus(&iBombinha_obj, OFF_STATE);
            printf_time();
        }
        vTaskDelay( 5000 / portTICK_PERIOD_MS );
    }
}
/*
void user_init() {
    uart_set_baud(0, 115200);
    inicialize_sntp();
    init_lcd();
    xTaskCreate(&qtest_bomba, "bomba", 1024, NULL, 2, NULL);
}
*/
