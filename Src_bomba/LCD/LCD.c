/*
 * Example of using HD44780 driver with LCD
 * connected directly to GPIO pins
 *
 * Part of esp-open-rtos
 * Copyright (C) 2016 Ruslan V. Uss <unclerus@gmail.com>
 * BSD Licensed as described in the file LICENSE
 */
#include "../include.h"
#include "../definition.h"

static const uint8_t char_data[] = {
    0x04, 0x0e, 0x0e, 0x0e, 0x1f, 0x00, 0x04, 0x00,
    0x1f, 0x11, 0x0a, 0x04, 0x0a, 0x11, 0x1f, 0x00
};

hd44780_t lcd = {
  .font = HD44780_FONT_5X8,
  .lines = 2,
  .pins = {
    .rs = PIN_RS,
    .e  = PIN_E,
    .d4 = PIN_D4,
    .d5 = PIN_D5,
    .d6 = PIN_D6,
    .d7 = PIN_D7,
    .bl = HD44780_NOT_USED
  }
};

void inicialize_sntp()
{
  char *servers[] = {SNTP_SERVERS};
  sntp_set_update_delay(1*60000);
  const struct timezone tz = {1*60, 0};
  sntp_initialize(&tz);
  sntp_set_servers(servers, sizeof(servers) / sizeof(char*));
}

void init_lcd()
{
  //inicialize_sntp();
  hd44780_init(&lcd);
  hd44780_upload_character(&lcd, 0, char_data);
  hd44780_upload_character(&lcd, 1, char_data + 8);
}

void clean()
{
  hd44780_gotoxy(&lcd, 0, 0);
  hd44780_puts(&lcd, "                        ");
  hd44780_gotoxy(&lcd, 0, 1);
  hd44780_puts(&lcd, "                        ");
}

void printf_nivel_agua(int nivel)
{
    char str[15]="";
    char aux[15];
    clean();
    hd44780_gotoxy(&lcd, 0, 0);
    sprintf(aux, "Nivel: (%d %%)", nivel*10);
    hd44780_puts(&lcd, aux);
    hd44780_gotoxy(&lcd, 0, 1);
    for(int i=0; i < nivel*1.5 ; i++)
    {
      strcat(str, "*");
    }
    hd44780_puts(&lcd, str);
}

void printf_time()
{
    time_t ts = time(NULL);
    struct tm * timeinfo;
    char time_str[80];
    char aux[40];
    timeinfo = gmtime (&ts);
    sprintf(time_str, "%s", asctime(timeinfo));
    int h = (timeinfo->tm_hour+24-4)%24;
    int m = timeinfo->tm_min;
    int d = timeinfo->tm_mday;
    int mo = timeinfo->tm_mon;
    clean();
    hd44780_gotoxy(&lcd, 0, 0);
    sprintf(aux, "   Data: %d/%d", d, mo);
    hd44780_puts(&lcd, aux);
    printf("%s\n", aux);
    hd44780_gotoxy(&lcd, 0, 1);
    sprintf(aux, "  Hora: %d:%d",h, m);
    printf("%s\n", aux);
    hd44780_puts(&lcd, aux);
}

void printf_status(int status)
{
    clean();
    char aux[40];
    sprintf(aux, " Status : ");
    if(status == ON_STATE)
      sprintf(aux, "%sON", aux);
    if(status == OFF_STATE)
      sprintf(aux, "%sOFF", aux);
    hd44780_gotoxy(&lcd, 0, 0);
    hd44780_puts(&lcd, aux);
}

void qtest_LCD(void *pvParameters)
{

    int j=0;
    while (true)
    {

        //printf_nivel_agua(j);
        printf_time();
        j = (j +1)%11;
        for (uint32_t i = 0; i < 1000; i++)
            sdk_os_delay_us(1000);
    }
}
/*
void user_init() {
    uart_set_baud(0, 115200);
    inicialize_sntp();
    init_lcd();
    xTaskCreate(&qtest_LCD, "LCD", 256, NULL, 2, NULL);
}
*/
