#include"../Message/message.c"

char str[30];
int isReturn;

void run_control(iBomba *iBombinha_obj, char MQTT_str[])
{
    int min, max;
    isReturn = 0;
    Message message;
    CRC_Obj crc;
    init_CRC(&crc);
    if(MQTT_str[POS_ID] == iBombinha_ID)
    {
        if(getCRC(&crc, MQTT_str) == 0)
        {
            switch (MQTT_str[POS_TYPE])
            {
                case GET_STATUS:
                    init_Message(&message, GET_STATUS);
                    getSTATUS(&message, *iBombinha_obj);
                    isReturn = 1;
                break;

                case ACTION:
                    //setStatus(iBombinha_obj, (uint8_t)MQTT_str[POS_DATA]);
                break;

                case CONFIG:
                    min = (uint8_t) MQTT_str[POS_DATA];
                    max = (uint8_t) MQTT_str[POS_DATA+1];
                    if(isConfigValid(min, max))
                    {
                        iBombinha_obj->nivel_min = min;
                        iBombinha_obj->nivel_max = max;
                    }
                    else
                    {
                        init_Message(&message, ERRO);
                        getERRO(&message, ERRO_CONFIG);
                        isReturn = 1;
                    }
                break;

                case GET_NIVEL:
                    if(MQTT_str[POS_DATA]!=GET_NIVEL)
                        iBombinha_obj->nivel_current = MQTT_str[POS_DATA];

                    else
                    {
                        init_Message(&message, GET_NIVEL);
                        getNivel(&message, *iBombinha_obj);
                        isReturn = 1;
                    }
                break;

            }
        }
        else
        {
            init_Message(&message, ERRO);
            getERRO(&message, ERRO_CRC);
            isReturn = 1;
        }
    }
    if(isReturn)
    {
        sprintf(str, "%s%s", message.head_str, message.data_str_local);
        sprintf(str, "%s%c", str, getCRC(&crc, str));
    }
}

void getStr(Message message_local)
{
  sprintf(str, "%s%s", message_local.head_str, message_local.data_str_local);
}

void qtestControl(void *pvParameters)
{
    iBomba iBombinha_obj;
    CRC_Obj crc;
    init_iBomba(&iBombinha_obj);
    init_CRC(&crc);
    init_nivel();
    char aux[15];
    while(1)
    {
        sensor_bomba(&iBombinha_obj);
        sprintf(aux, "%c%c%c%c", iBombinha_ID, 0x01, GET_NIVEL, GET_NIVEL);
        sprintf(aux, "%s%c", aux, getCRC(&crc, aux));
        printf_message_str(aux);
        printf("\n----------------------------------------------\n");
        run_control(&iBombinha_obj, aux);
        if(isReturn)printf_message_str(str);
        printf("Min= %d  Max= %d\n", iBombinha_obj.nivel_min, iBombinha_obj.nivel_max);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
/*
void user_init(void)
{
	 uart_set_baud(0, 115200);
   xTaskCreate(&qtestControl, "temp_control", 1024, NULL, 2, NULL);
}
*/
