#include "Bomba.h"

void init_iBomba(iBomba * iBomba_obj)
{
    gpio_enable(PIN_iBOMBA, GPIO_OUTPUT);
    iBomba_obj->nivel_min = 10;
    iBomba_obj->nivel_max = 40;
    iBomba_obj->nivel_current = 20;
    iBomba_obj->state = OFF_STATE;
}

void setStatus(iBomba *iBomba_obj, uint8_t action)
{
    if(action == ON_STATE)
    {
        gpio_write(PIN_iBOMBA, 1);
    }
    if(action == OFF_STATE)
    {
        gpio_write(PIN_iBOMBA, 0);
    }
}

int isConfigValid(int min, int max)
{
    return max > min && min>=0 && max <=100;
}

void sensor_bomba(iBomba *iBomba_obj)
{
    iBomba_obj->nivel_current = 1;
    if(gpio_read(PIN_NIVEL_10))
      iBomba_obj->nivel_current = 10;
    if(gpio_read(PIN_NIVEL_20))
      iBomba_obj->nivel_current = 20;
    if(gpio_read(PIN_NIVEL_30))
      iBomba_obj->nivel_current = 30;
    //if(gpio_read(PIN_NIVEL_40))
    //  iBomba_obj->nivel_current = 40;
    if(gpio_read(PIN_NIVEL_50))
      iBomba_obj->nivel_current = 50;
    if(gpio_read(PIN_NIVEL_60))
      iBomba_obj->nivel_current = 60;
    if(gpio_read(PIN_NIVEL_70))
      iBomba_obj->nivel_current = 70;
    if(gpio_read(PIN_NIVEL_80))
      iBomba_obj->nivel_current = 80;
      /*
    if(gpio_read(PIN_NIVEL_90))
      iBomba_obj->nivel_current = 90;
    //if(gpio_read(PIN_NIVEL_100))
    //  iBomba_obj->nivel_current = 100;
    iBomba_obj->nivel_current = 90; */
}

void qtest_bomba(void *pvParameters)
{
    iBomba iBombinha_obj;
    init_iBomba(&iBombinha_obj);
    printf("Iniciando a bomba:");
    while(1)
    {
        sensor_bomba(&iBombinha_obj);
        printf("NIVEL = %d\n", iBombinha_obj.nivel_current);
        vTaskDelay( 5000 / portTICK_PERIOD_MS );
    }
}

void init_nivel()
{
//  gpio_enable(PIN_NIVEL_00, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_10, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_20, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_30, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_40, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_50, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_60, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_70, GPIO_INPUT);
  gpio_enable(PIN_NIVEL_80, GPIO_INPUT);
  //gpio_enable(PIN_NIVEL_90, GPIO_INPUT);
  //gpio_enable(PIN_NIVEL_100, GPIO_INPUT);
}


void user_init() {
    uart_set_baud(0, 115200);
    init_nivel();
    xTaskCreate(&qtest_bomba, "bomba", 2048, NULL, 2, NULL);
}
