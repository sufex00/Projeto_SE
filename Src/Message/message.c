#include"message.h"


void getHead(Message *message)
{
    if(message->typeMessage == CONFIG)
    {
        message->size = SIZE_CONFIG;
    }
    if(message->typeMessage == ACTION)
    {
        message->size = SIZE_ACTION;
    }
    if(message->typeMessage == ERRO)
    {
        message->size = SIZE_ERRO;
    }
    if(message->typeMessage == GET_STATUS)
    {
        message->size = SIZE_GET_STATUS;
    }
    if(message->typeMessage == GET_NIVEL)
    {
        message->size = SIZE_GET_NIVEL;
    }
    sprintf(message->head_str, "%c%c%c", iBombinha_ID, message->size, message->typeMessage);
}

void init_Message(Message *message, uint8_t typeMessage)
{
  message->typeMessage = typeMessage;
  getHead(message);
}

void getSTATUS(Message *message_local, iBomba iBombinha)
{
    sprintf(message_local->data_str_local, "%c",iBombinha.state);
}

void getERRO(Message *message_local,uint8_t ERRO_STATUS)
{
  sprintf(message_local->data_str_local, "%c", ERRO_STATUS);
}

void getNivel(Message *message_local, iBomba iBombinha)
{
  sprintf(message_local->data_str_local, "%c%c%c", iBombinha.nivel_current, iBombinha.nivel_min, iBombinha.nivel_max);
}

void getACTION(Message *message_local, uint8_t ACTION_NUM)
{
  sprintf(message_local->data_str_local, "%c", ACTION_NUM);
}

void printf_message(Message message_local)
{
    int i,size;
    for(i = 0 ; i < SIZE_HEAD ; i ++)
    {
      printf("%x | ", (uint8_t)message_local.head_str[i]);
    }
    size = strlen(message_local.data_str_local);
    printf("-> ");
    for(i = 0 ; i < size ; i ++)
    {
      printf("%x | ", message_local.data_str_local[i]);
    }
}

void printf_message_str(char str [])
{
  int i, size;
  size = strlen(str);
  for (i = 0; i < size; i++)
  {
      printf("%x | ", str[i]);
  }
  printf("\n");
}

void qtestMessage(void *pvParameters)
{
  iBomba iBombinha_obj;
  Message message_obj;
  init_iBomba(&iBombinha_obj);
  init_Message(&message_obj, ERRO);

  while(1)
  {
      getERRO(&message_obj, ERRO_CONFIG);
      printf_message(message_obj);
      vTaskDelay( 5000 / portTICK_PERIOD_MS );
  }
}
/*
void user_init(void)
{
	 uart_set_baud(0, 115200);
   xTaskCreate(&qtestMessage, "temp_message", 1024, NULL, 2, NULL);
}
*/
