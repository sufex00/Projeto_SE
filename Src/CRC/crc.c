#include"crc.h"

void init_CRC(CRC_Obj *crc_local)
{
  int temp;
  for(int i=0 ; i < 256 ; i ++)
  {
      crc_local->CRC[i] = 0;
  }
  for(int i=0 ; i<256 ; i++)
  {
      temp = i;
      for(int j=0 ; j < 8 ;j++)
      {
          if ((temp & 0x80) != 0)
              temp = (temp << 1) ^ CRC_POLYNOMIAL;
          else
              temp = temp << 1;
      }
      crc_local->CRC[i] = temp ;
  }
}

uint8_t getCRC(CRC_Obj *crc_local, char str[])
{
    uint8_t crc = 0;
    int size;
    size = strlen(str);
    if(size > 0)
    {
        for(int i = 0 ; i < size; i ++)
        {
          uint8_t byte = (uint8_t) str[i];
          crc = crc_local->CRC[crc ^ byte];
        }
    }
    return crc;
}

void qtestCRC(void *pvParameters)
{
    CRC_Obj crc;
    char mens[10] = {0x51, 0x03, 0x01, 0x02, 0x26, 0x26};
    init_CRC(&crc);
    printf("CRC = %d",getCRC(&crc, mens));
    sprintf(mens, "%s%c", mens, getCRC(&crc, mens));
    printf("\nCRC = %d",getCRC(&crc, mens));

}

/*
void user_init(void)
{
	uart_set_baud(0, 115200);
  xTaskCreate(&qtestCRC, "temp_CRC", 512, NULL, 2, NULL);
}
*/
